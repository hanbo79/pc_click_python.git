# -*- coding: utf-8 -*-
import datetime
import time

import ntplib
import requests


def get_beijin_time():
    try:
        url = 'https://beijing-time.org/'
        request_result = requests.get(url=url)
        if request_result.status_code == 200:
            headers = request_result.headers
            net_date = headers.get("date")
            gmt_time = time.strptime(net_date[5:25], "%d %b %Y %H:%M:%S")
            bj_timestamp = int(time.mktime(gmt_time) + 8 * 60 * 60)
            return datetime.datetime.fromtimestamp(bj_timestamp)
    except Exception as exc:
        return  datetime.datetime.now()


def get_ntp_time():
    ntp_client = ntplib.NTPClient()
    response = ntp_client.request('cn.ntp.org.cn')
    return datetime.datetime.fromtimestamp(response.tx_time)

def checktimeout():
    now = get_beijin_time()
    print("当前时间: ", now)
    timeStamp = datetime.datetime(2023, 10, 1, 0, 0, 0, 0)   #指定时间
    print("stattime:",timeStamp)
    endtime = timeStamp + datetime.timedelta(days=20)
    if(endtime < now):
        print("timeout,max:20天")
        return True
    return False

if __name__ == '__main__':
    checktimeout()
